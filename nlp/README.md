#README

This module is build as an extention of work done with Mulu Adhana and Yusra Ibrahim at UPB in Bucharest.

##How to
Run in the following way:   
1. from nlp_feature_extractor import *    
2. nlpfeat = NlpFeatureExtractor(<input_text>)   
3. result = nlpfeat.get_result()   

##created by Lukas Toma, 2015